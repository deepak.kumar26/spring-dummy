package com.springrest.fileupload.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.springrest.fileupload.entity.Application;

import com.springrest.fileupload.entity.Verification;
import org.springframework.stereotype.Component;

@Component
public class ApplicationService {


    private static List<Application> appList = new ArrayList<>();


//    ne();

    static {
        appList.add(new Application("APP0000001","test2",200000,"Home Loan","New Loan","Jaipur HO","Jaipur","Rajesthan"));
        appList.add(new Application("APP0000002","test3",204500,"Car Loan","New Loan","Jaipur HO","Jaipur","Rajesthan"));
        appList.add(new Application("APP0000013","test4",340000,"Home Loan","New Loan","Jaipur HO","Jaipur","Rajesthan"));
    }

    //get all details

    public List<Application> getDetails() {

        return appList;
    }

    // gets all the details by the application number provided to the api.


    public Application getDetailsByID(String id) {

        Application applicant = null;
        // System.out.println(id);
        try{
            applicant = appList.stream().filter(e->e.getApplicationNo().equals(id)).findFirst().get();
        } catch (Exception e) {
//            System.err.println(e);
            e.printStackTrace();
        }
        return applicant;
    }

    public HashMap<String, Object> getVerification() throws  Exception {
        HashMap<String, Object> tstObj = new HashMap<String, Object>();
        HashMap<Object,Verification> pending_ver = new HashMap<Object, Verification>();

        pending_ver.put("verification", new Verification(03,23,04,13));
//        pending_ver.put("deviation",new Verification("","","",""));
//        pending_ver.put("send_for_vecation",12);
//        pending_ver.put("send_for_vjerification",12);


        tstObj.put("Verification_status", pending_ver );

//        tstObj.put("deviation", );


        return tstObj;
    }

}
