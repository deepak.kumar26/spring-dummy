package com.springrest.fileupload.entity;

//import java.util.List;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Setter
@Getter
@Entity
public class Verification {

    public Verification(Integer send_for_verification, Integer pending_for_submition, Integer suggested_edit, Integer deviation_pending) {
        this.send_for_verification = send_for_verification;
        this.pending_for_submition = pending_for_submition;
        this.suggested_edit = suggested_edit;
        this.deviation_pending = deviation_pending;
    }

    public Verification() {

    }

    public Integer getSend_for_verification() {
        return send_for_verification;
    }

    public void setSend_for_verification(Integer send_for_verification) {
        this.send_for_verification = send_for_verification;
    }

    public Integer getPending_for_submition() {
        return pending_for_submition;
    }

    public void setPending_for_submition(Integer pending_for_submition) {
        this.pending_for_submition = pending_for_submition;
    }

    public Integer getSuggested_edit() {
        return suggested_edit;
    }

    public void setSuggested_edit(Integer suggested_edit) {
        this.suggested_edit = suggested_edit;
    }

    public Integer getDeviation_pending() {
        return deviation_pending;
    }

    public void setDeviation_pending(Integer deviation_pending) {
        this.deviation_pending = deviation_pending;
    }

    @Override
    public String toString() {
        return "Verification{" +
                "send_for_verification=" + send_for_verification +
                ", pending_for_submition=" + pending_for_submition +
                ", suggested_edit=" + suggested_edit +
                ", deviation_pending=" + deviation_pending +
                '}';
    }

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    private Integer send_for_verification;
    private Integer pending_for_submition;
    private Integer suggested_edit;
    private Integer deviation_pending;


}

