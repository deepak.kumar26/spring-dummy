package com.springrest.fileupload.entity;


import com.fasterxml.jackson.annotation.JsonRootName;

//@JsonRootName("application_list")
public class Application {

    private String applicationNo;
    private String applicantName;
    private Integer loanAmnt;
    private String productType;
    private String loanType;
    private String branch;
    private String city;
    private String state;

    
    public String getApplicantName() {
        return applicantName;
    }
    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }
    public Integer getLoanAmnt() {
        return loanAmnt;
    }
    public void setLoanAmnt(Integer loanAmnt) {
        this.loanAmnt = loanAmnt;
    }
    public String getProductType() {
        return productType;
    }
    public void setProductType(String productType) {
        this.productType = productType;
    }
    public String getLoanType() {
        return loanType;
    }
    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }
    public String getBranch() {
        return branch;
    }
    public void setBranch(String branch) {
        this.branch = branch;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }
    public String getApplicationNo() {
        return applicationNo;
    }
    public void setApplicationNo(String applicationNo) {
        this.applicationNo = applicationNo;
    }
    
    
    
    @Override
    public String toString() {
        return "Application [Application Name=" + applicantName + ", applicationNo=" + applicationNo + ", branch=" + branch
                + ", city=" + city + ", loanAmnt=" + loanAmnt + ", loanType=" + loanType + ", productType="
                + productType + ", state=" + state + "]";
    }
    
    public Application(String applicationNo, String applicantName, Integer loanAmnt, String productType,
            String loanType, String branch, String city, String state) {        
            this.applicationNo = applicationNo;
            this.applicantName = applicantName;
            this.loanAmnt = loanAmnt;
            this.productType = productType;
            this.loanType = loanType;
            this.branch = branch;
            this.city = city;
            this.state = state;
    }
    
    public Application() {
    }
    

}
