package com.springrest.fileupload.controller;


import static java.lang.System.out;

import java.io.IOException;

import com.springrest.fileupload.helper.PDFConvertor;

//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class FileUploadController {

    private final PDFConvertor pdfconvertor;

    public FileUploadController(PDFConvertor pdfconvertor) {
        this.pdfconvertor = pdfconvertor;
    }

    @PostMapping("/upload-file")
    public ResponseEntity<String> uploadFile(@RequestParam("image") MultipartFile file) throws IOException {

//        out.println(file.getContentType());
//        out.println(file.getOriginalFilename());
        out.println(file);
        // String fileName = file.getOriginalFilename();

        String chk = "null";
        try{
            //validation
            if(file.isEmpty()){
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Must Contain File.");
            }

            if(file.getContentType().equals("application/pdf")){
                this.pdfconvertor.generateImageFromPDF("C:\\Users\\Asus\\Downloads\\testingpdf.pdf","jpg");
                chk = "pdf is get";
            }else{
                chk = "just checking";
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return ResponseEntity.ok(chk);
    }

    @GetMapping("/hello-world")
    public ResponseEntity<String> welcome(){
        return ResponseEntity.ok().body("Hello world");
    }


}
